from rest_framework import serializers
from Server.models import Actor
from django.contrib.auth.models import User

class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('id', 'username', 'last_login', 'first_name', 'last_name', 'email')


class ActorSerializer(serializers.ModelSerializer):

    user = UserSerializer(read_only=True)

    class Meta:
        model = Actor
        fields = ('id', 'user')

