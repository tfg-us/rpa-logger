from rest_framework.response import Response
# from rest_framework.permissions import IsAuthenticated
from django.contrib.auth.models import update_last_login
from rest_framework.authtoken.models import Token
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.serializers import AuthTokenSerializer
from rest_framework import status
from login.serializers import ActorSerializer
from django.contrib.auth.models import User
from Server.models import Actor
from django.db import transaction
from drf_yasg.utils import swagger_auto_schema
from rest_framework.parsers import MultiPartParser
from rest_framework.decorators import action
from rest_framework import generics

class LoginManager(generics.CreateAPIView):
    # permission_classes = (IsAuthenticated,)
    queryset = User.objects.all()
    serializer_class = AuthTokenSerializer
    parser_classes = (MultiPartParser,)

    @swagger_auto_schema(operation_description='Check if a template is in the screenshots of the project.', )
    @transaction.atomic
    def post(self, request):
        authTokenSerializer = AuthTokenSerializer(data=request.data)
        if authTokenSerializer.is_valid(raise_exception=True):
            user = authTokenSerializer.validated_data['user']
            last_login = user.last_login
            update_last_login(None, user)
            token, created = Token.objects.get_or_create(user=user)
            if token.key is not None:
                headers = {'x-auth': token.key}
                return Response({"token":token.key}, status=status.HTTP_200_OK, headers=headers)
            else:
                return Response({"User not found"}, status=status.HTTP_400_BAD_REQUEST)


class AdminLoginManager(ObtainAuthToken):
    # permission_classes = (IsAuthenticated,)
    queryset = User.objects.all()
    # serializer_class = UserSerializer

    @transaction.atomic
    def post(self, request):
        authTokenSerializer = AuthTokenSerializer(data=request.data)
        if authTokenSerializer.is_valid(raise_exception=True):
            user = authTokenSerializer.validated_data['user']
            last_login = user.last_login
            update_last_login(None, user)
            token, created = Token.objects.get_or_create(user=user)
            if token.key is not None:
                headers = {'x-auth': token.key}
                if Actor.objects.filter(user_id=token.user.id).first():
                    admin = Actor.objects.get(user_id=token.user.id)
                    admin.user.last_login = last_login
                    serialized = ActorSerializer(admin)
                else:
                    return Response({"Admin not found"}, status=status.HTTP_400_BAD_REQUEST)
                return Response(serialized.data, status=status.HTTP_200_OK, headers=headers)
            else:
                return Response({"Token not get/created"}, status=status.HTTP_400_BAD_REQUEST)