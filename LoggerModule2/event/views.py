from rest_framework import status
from Server.models import Screenshot, Computer, Capture
from rest_framework import generics
from rest_framework.response import Response
from utils.authentication_utils import get_token_computer
from utils.Assertions import Assertions
from .serializers import *
import json
from django.db import transaction
from drf_yasg.utils import swagger_auto_schema


class CreateEvent(generics.CreateAPIView):
    serializer_class = EventSerializer
    swagger_schema = None

    @swagger_auto_schema(operation_description="Agregar un evento a una captura activa por un ordenador.")
    @transaction.atomic
    def post(self, request, *args, **kwargs):
        loggedComputer = get_token_computer(request)
        if loggedComputer is not None:
            capture = Capture.objects.filter(computer=loggedComputer,saveEvent=False).first()
            if capture is not None:
                datas = request.data
                for i in datas:
                    i["capture"] = capture.id
                    serializer = EventSerializer(data=i)
                    if serializer.validate(i):
                        serializer.is_valid()
                        serializer.save()
                if not capture.status:
                    capture.saveEvent = True
                capture.save()
                return Response(status=status.HTTP_201_CREATED)
            else:
                Assertions.assert_true_raise(False, 404, "Capture not found")
        else:
            return Response(status=status.HTTP_401_UNAUTHORIZED)
