from rest_framework import serializers
from Server.models import CapturePolicy

class ShortCaptureConfigurationSerializer(serializers.ModelSerializer):

    class Meta:
        model = CapturePolicy
        fields = ('id','name','actions')


