from rest_framework import status
from Server.models import Project, Configuration, Screenshot, Event
from rest_framework import generics
from rest_framework.response import Response
from utils.authentication_utils import get_logged_user, get_server_token_access
from utils.Assertions import Assertions
from .serializers import *
from drf_yasg.utils import swagger_auto_schema


class ProjectManager(generics.RetrieveUpdateAPIView):
    queryset = Project.objects.all()
    serializer_class = ProjectSerializer

    def get_object(self, pk=None):
        if pk is None:
            pk = self.kwargs['pk']
        try:
            return Project.objects.get(pk=pk)
        except Project.DoesNotExist:
            Assertions.assert_true_raise(False,404,"Not found")

    @swagger_auto_schema(operation_description="Get the project by id.")
    def get(self, request, pk=None, format=None):
        loggedUser = get_logged_user(request)
        serverRPA = get_server_token_access(request, "RPA")
        if loggedUser is not None or serverRPA is not None:
            if pk is None:
                pk = self.kwargs['pk']
            project = self.get_object(pk)
            serializer = ProjectSerializer(project)
            return Response(serializer.data)

    @swagger_auto_schema(operation_description="Modify the project by id.")
    def put(self, request, pk=None):
        loggedUser = get_logged_user(request)
        if loggedUser is not None:
            try:
                if pk is None:
                    pk = self.kwargs['pk']
                project = self.get_object(pk)
                serializer = ProjectSerializer(project, data=request.data, partial=True)
                if serializer.is_valid():
                    serializer.save()
                    return Response(serializer.data, status=status.HTTP_200_OK)
                else:
                    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
            except:
                return Response(status=status.HTTP_400_BAD_REQUEST)
        else:
            Assertions.assert_true_raise(False,404,"User Not Found")

    @swagger_auto_schema(operation_description="Delete the project by id.")
    def delete(self, request, pk=None, format=None):
        loggedUser = get_logged_user(request)
        if loggedUser is not None:
            if pk is None:
                pk = self.kwargs['pk']
            project = self.get_object(pk)
            if project is not None:
                configuration = Configuration.objects.filter(project=project).first()
                if configuration is None:
                    events = Event.objects.filter(capture__project=project)
                    screenshots = Screenshot.objects.filter(capture__project=project)
                    captures = Capture.objects.filter(project=project)
                    events.delete()
                    screenshots.delete()
                    captures.delete()
                    project.delete()
                    return Response(status=status.HTTP_204_NO_CONTENT)
                else:
                    return Response(status=status.HTTP_406_NOT_ACCEPTABLE)
            else:
                return Response(status=status.HTTP_404_NOT_FOUND)
        else:
            return Response(status=status.HTTP_401_UNAUTHORIZED)

class ListProject(generics.ListCreateAPIView):
    serializer_class = ProjectSerializer

    def get_queryset(self):
        return Project.objects.all()

    @swagger_auto_schema(operation_description="List the projects.")
    def get(self, request):
        loggedUser = get_logged_user(request)
        serverRPA = get_server_token_access(request, "RPA")
        if loggedUser is not None or serverRPA is not None:
            queryset = Project.objects.all()
            serializer = ShortProjectSerializer(queryset, many=True)
            return Response(serializer.data)

    @swagger_auto_schema(operation_description="Create a project.")
    def post(self, request, *args, **kwargs):
        loggedUser = get_logged_user(request)
        if loggedUser is not None:
            serializer = ProjectSerializer(data=request.data,partial=True)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            else:
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        else:
            Assertions.assert_true_raise(False,404,"User not found")
