from rest_framework import status
from Server.models import Computer
from rest_framework import generics
from rest_framework.response import Response
from utils.authentication_utils import get_logged_user
from utils.Assertions import Assertions
from .serializers import *
from drf_yasg.utils import swagger_auto_schema
from drf_yasg import openapi


class ComputerManager(generics.RetrieveUpdateAPIView):
    queryset = Computer.objects.all()
    serializer_class = ComputerSerializer

    def get_object(self, pk=None):
        if pk is None:
            pk = self.kwargs['pk']
        try:
            return Computer.objects.get(pk=pk)
        except Computer.DoesNotExist:
            Assertions.assert_true_raise(False,404,"Not found")

    @swagger_auto_schema(operation_description="Get a computer by id.")
    def get(self, request, pk=None, format=None):
        loggedUser = get_logged_user(request)
        if loggedUser is not None:
            if pk is None:
                pk = self.kwargs['pk']
            computer = self.get_object(pk)
            serializer = ComputerSerializer(computer)
            return Response(serializer.data)
        else:
            return Response(status=status.HTTP_401_UNAUTHORIZED)

    @swagger_auto_schema(operation_description="Modify a computer by id. To disable the computer, change it to actived false.")
    def put(self, request, pk=None):
        loggedUser = get_logged_user(request)
        if loggedUser is not None:
            try:
                request.data["MAC"] =  (request.data["MAC"]).upper()
                if pk is None:
                    pk = self.kwargs['pk']
                computer = self.get_object(pk)
                if computer is not None:
                    serializer = ComputerSerializer(computer, data=request.data, partial=True)
                    validate_MAC(request.data, pk)
                else:
                    return Response(status=status.HTTP_400_BAD_REQUEST)
                if serializer.is_valid():
                    serializer.save()
                    return Response(serializer.data)
                else:
                    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
            except:
                return Response(status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response(status=status.HTTP_401_UNAUTHORIZED)


class ListComputers(generics.ListCreateAPIView):
    serializer_class = ComputerSerializer
    project = openapi.Parameter('project', openapi.IN_QUERY, description="List by project id.", type=openapi.TYPE_INTEGER)


    @swagger_auto_schema(operation_description="List computers.",manual_parameters=[project])
    def get(self, request):
        loggedUser = get_logged_user(request)
        if loggedUser is not None:
            try:
                projectId = request.GET['project']
                queryset = Computer.objects.filter(project=projectId)
            except:
                queryset = Computer.objects.all()
            serializer = ComputerSerializer(queryset, many=True)
            return Response(serializer.data)
        else:
            return Response(status=status.HTTP_401_UNAUTHORIZED)

    @swagger_auto_schema(operation_description="Create a computer.")
    def post(self, request, *args, **kwargs):
        loggedUser = get_logged_user(request)
        try:
            if loggedUser is not None:
                request.data["MAC"] =  (request.data["MAC"]).upper()
                serializer = ComputerSerializer(data=request.data,partial=True)
                validate_MAC(request.data, None)
                if serializer.is_valid():
                    serializer.save()
                    return Response(serializer.data, status=status.HTTP_201_CREATED)
                else:
                    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
            else:
                return Response(status=status.HTTP_401_UNAUTHORIZED)
        except:
            return Response(status=status.HTTP_400_BAD_REQUEST)
