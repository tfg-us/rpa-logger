from rest_framework import serializers
from Server.models import Computer
from project.serializers import ShortProjectSerializer
from utils.Assertions import *
from utils.strings import Strings

class ComputerSerializer(serializers.ModelSerializer):

    class Meta:
        model = Computer
        fields = ('id','MAC','RAM','primaryDisk','machine','processor','core','system','release','version','namePC','userPC','active')

    def validate(self, attrs):

            data = attrs


            if data.get("RAM"):
                Assertions.assert_true_raise(Strings.check_number_max(float(data.get('RAM')), 1000000000.00),400, ('ERROR_RAM'))
            if data.get("primaryDisk"):
                Assertions.assert_true_raise(Strings.check_number_max(float(data.get('primaryDisk')), 1000000000.00),400,('ERROR_PRIMARY_DISK'))
            if data.get("core"):
                Assertions.assert_true_raise(Strings.check_number_max(int(data.get('core')), 1000000000),400, ('ERROR_CORE'))
            if data.get("machine"):
                Assertions.assert_true_raise(Strings.check_max_length(data.get('machine'), 254),400,('ERROR_MACHINE'))
            if data.get("processor"):
                Assertions.assert_true_raise(Strings.check_max_length(data.get('processor'), 254),400,('ERROR_PROCESSOR'))
            if data.get("system"):
                Assertions.assert_true_raise(Strings.check_max_length(data.get('system'), 254),400,('ERROR_SYSTEM'))
            if data.get("release"):
                Assertions.assert_true_raise(Strings.check_max_length(data.get('release'), 254),400,('ERROR_RELEASE'))
            if data.get("version"):
                Assertions.assert_true_raise(Strings.check_max_length(data.get('version'), 254),400,('ERROR_VERSION'))
            if data.get("namePC"):
                Assertions.assert_true_raise(Strings.check_max_length(data.get('namePC'), 254),400,('ERROR_NAMEPC'))
            if data.get("userPC"):
                Assertions.assert_true_raise(Strings.check_max_length(data.get('userPC'), 254),400,('ERROR_USERPC'))
            return data

def validate_MAC(data,pk):
    if data.get("MAC"):
        Assertions.assert_true_raise(Strings.check_mac(data.get('MAC')),400, ('ERROR_MAC'))
        computers = Computer.objects.filter(MAC=data.get("MAC")).first()
        if pk is not None and computers is not None:
            Assertions.assert_true_raise((computers.id == int(pk)),400, ('ERROR_MAC_EXIST'))
        else:
            Assertions.assert_true_raise((computers is None), 400, ('ERROR_MAC_EXIST'))