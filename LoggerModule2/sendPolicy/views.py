from rest_framework import status
from Server.models import SendPolicy
from rest_framework import generics
from rest_framework.response import Response
from utils.authentication_utils import get_logged_user
from utils.Assertions import Assertions
from .serializers import *
from drf_yasg.utils import swagger_auto_schema


class SendPolicyManager(generics.RetrieveUpdateAPIView):
    queryset = SendPolicy.objects.all()
    serializer_class = SendPolicySerializer

    def get_object(self, pk=None):
        if pk is None:
            pk = self.kwargs['pk']
        try:
            return SendPolicy.objects.get(pk=pk)
        except SendPolicy.DoesNotExist:
            Assertions.assert_true_raise(False,404,"Not found")

    @swagger_auto_schema(operation_description="Get the send policy by id.")
    def get(self, request, pk=None, format=None):
        loggedUser = get_logged_user(request)
        if loggedUser is not None:
            if pk is None:
                pk = self.kwargs['pk']
            sendPolicy = self.get_object(pk)
            serializer = SendPolicySerializer(sendPolicy)
            return Response(serializer.data)

    @swagger_auto_schema(operation_description="Modify the send policy by id.")
    def put(self, request, pk=None):
        loggedUser = get_logged_user(request)
        if loggedUser is not None:
            if pk is None:
                pk = self.kwargs['pk']
            sendPolicy = self.get_object(pk)
            serializer = SendPolicySerializer(sendPolicy, data=request.data, partial=True)
            default_validator(pk, request.data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_200_OK)
            else:
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        else:
            Assertions.assert_true_raise(False,404,"User Not Found")

    @swagger_auto_schema(operation_description="Delete the send policy by id and change the projects to the default send policy.")
    def delete(self, request, pk=None, format=None):
        loggedUser = get_logged_user(request)
        if loggedUser is not None:
            if pk is None:
                pk = self.kwargs['pk']
            sendPolicy = self.get_object(pk)
            if sendPolicy.default is True:
                return Response(status=status.HTTP_409_CONFLICT)
            else:
                if sendPolicy is not None:
                    project = Project.objects.filter(sendPolicy=sendPolicy)
                    sendPolicy_default = SendPolicy.objects.filter(default=True).first()
                    if project:
                        for i in project:
                            i.sendPolicy = sendPolicy_default
                            i.save()
                    sendPolicy.delete()
                    return Response(status=status.HTTP_204_NO_CONTENT)
        else:
            return Response(status=status.HTTP_401_UNAUTHORIZED)

class ListSendPolicy(generics.ListCreateAPIView):
    serializer_class = SendPolicySerializer

    def get_queryset(self):
        return SendPolicy.objects.all()

    @swagger_auto_schema(operation_description="List the send policy.")
    def get(self, request):
        loggedUser = get_logged_user(request)
        if loggedUser is not None:
            queryset = SendPolicy.objects.all()
            serializer = SendPolicySerializer(queryset, many=True)
            return Response(serializer.data)

    @swagger_auto_schema(operation_description="Create a send policy. There can only be one default. If a default policy exist, modify it.")
    def post(self, request, *args, **kwargs):
        loggedUser = get_logged_user(request)
        if loggedUser is not None:
            serializer = SendPolicySerializer(data=request.data,partial=True)
            default_validator(None, request.data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            else:
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        else:
            Assertions.assert_true_raise(False,404,"User not found")
