from rest_framework import status
from rest_framework import generics
from rest_framework.response import Response
from .serializers import *
from django.http import HttpResponse
from utils.authentication_utils import get_logged_user
from drf_yasg.utils import swagger_auto_schema
from rest_framework.parsers import MultiPartParser
from rest_framework.decorators import action

class DownloadSimilarityImage(generics.RetrieveAPIView):
    queryset = Similarity.objects.all()
    serializer_class = SimilaritySerializer

    @swagger_auto_schema(operation_description="Download a similarity image by image name.")
    def get(self, request, filename=None, format=None):
        loggedUser = get_logged_user(request)
        if loggedUser is not None:
            if filename is None:
                filename = self.kwargs['pk']
            similarity = Similarity.objects.filter(picturePath="configuration/similarities/"+filename).first()
            if similarity is not None:
                filename = similarity.picturePath.file.name.split('\\')[-1]
                response = HttpResponse(similarity.picturePath.file, content_type='text/plain')
                response['Content-Disposition'] = 'attachment; filename=%s' % filename
            else:
                response = Response(status=status.HTTP_404_NOT_FOUND)
            return response
        else:
            return Response(status=status.HTTP_401_UNAUTHORIZED)


class SimilarityManager(generics.DestroyAPIView):
    queryset = Similarity.objects.all()

    def get_object(self, pk=None):
        if pk is None:
            pk = self.kwargs['pk']
        try:
            return Similarity.objects.get(pk=pk)
        except Similarity.DoesNotExist:
            Assertions.assert_true_raise(False,404,"Not found")

    @swagger_auto_schema(operation_description="Delete a similarity by id.")
    def delete(self, request, pk=None, format=None):
        loggedUser = get_logged_user(request)
        if loggedUser is not None:
            if pk is None:
                pk = self.kwargs['pk']
            similarity = self.get_object(pk)
            similarity.delete()
            return Response(status=status.HTTP_204_NO_CONTENT)
        else:
            return Response(status=status.HTTP_401_UNAUTHORIZED)


class ListSimilarity(generics.CreateAPIView):
    serializer_class = SimilaritySerializer

    def get_queryset(self):
        return Similarity.objects.all()

    parser_classes = (MultiPartParser,)

    @swagger_auto_schema(operation_description='Check if a template is in the screenshots of the project.', )
    @action(detail=False, methods=['post'])
    def post(self, request, *args, **kwargs):

        loggedUser = get_logged_user(request)
        if loggedUser is not None:
            serializer = SimilaritySerializer(data=request.data, partial=True)
            try:
                if serializer.is_valid():
                    similarity = serializer.save()
                    if processing_data_similarity(similarity):
                        return Response(serializer.data, status=status.HTTP_201_CREATED)
                else:
                    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
            except:
                return Response(serializer.data, status=status.HTTP_417_EXPECTATION_FAILED)
        else:
            return Response(status=status.HTTP_401_UNAUTHORIZED)

