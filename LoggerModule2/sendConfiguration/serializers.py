from rest_framework import serializers
from Server.models import SendPolicy


class ShortSendConfigurationSerializer(serializers.ModelSerializer):

    class Meta:
        model = SendPolicy
        fields = ('id','name','eventNumber','imageLimit')



