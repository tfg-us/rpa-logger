from rest_framework import serializers
from Server.models import Delete, Group
from project.serializers import ShortProjectSerializer
from utils.Assertions import *
from utils.strings import Strings
from django.db import transaction


class DeleteSerializer(serializers.ModelSerializer):

    class Meta:
        model = Delete
        fields = ('id','group')

    '''
    Save a delete group and validate it
    '''
    @transaction.atomic
    def save(self, **kwargs):
        data = self.data
        try:
            pk = data["id"]
        except:
            pk = None
        group = Group.objects.filter(pk=data["group"]).first()
        if pk is None:
            delete = Delete(group=group)
        else:
            delete = Delete(pk=pk,group=group)
        delete = delete.save()
        return delete