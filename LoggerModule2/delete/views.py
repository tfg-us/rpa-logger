from rest_framework import status
from Server.models import Delete
from rest_framework import generics
from rest_framework.response import Response
from utils.authentication_utils import get_logged_user
from utils.Assertions import Assertions
from .serializers import *
from drf_yasg.utils import swagger_auto_schema
from drf_yasg import openapi


class DeleteManager(generics.RetrieveAPIView):
    queryset = Delete.objects.all()
    serializer_class = DeleteSerializer

    def get_object(self, pk=None):
        if pk is None:
            pk = self.kwargs['pk']
        try:
            return Delete.objects.get(pk=pk)
        except Delete.DoesNotExist:
            Assertions.assert_true_raise(False,404,"Not found")

    @swagger_auto_schema(operation_description="Get a delete/group by id.")
    def get(self, request, pk=None, format=None):
        loggedUser = get_logged_user(request)
        if loggedUser is not None:
            if pk is None:
                pk = self.kwargs['pk']
            delete = self.get_object(pk)
            serializer = DeleteSerializer(delete)
            return Response(serializer.data)
        else:
            return Response(status=status.HTTP_401_UNAUTHORIZED)

    @swagger_auto_schema(operation_description="Delete a delete/group by id.")
    def delete(self, request, pk=None, format=None):
        loggedUser = get_logged_user(request)
        if loggedUser is not None:
            if pk is None:
                pk = self.kwargs['pk']
            delete = self.get_object(pk)
            delete.delete()
            return Response(status=status.HTTP_204_NO_CONTENT)
        else:
            return Response(status=status.HTTP_401_UNAUTHORIZED)

class ListDelete(generics.ListCreateAPIView):
    serializer_class = DeleteSerializer

    def get_queryset(self):
        return Delete.objects.all()
    configuration = openapi.Parameter('configuration', openapi.IN_QUERY, description="List by configuration id.", type=openapi.TYPE_INTEGER)

    @swagger_auto_schema(operation_description="List equals/groups.",manual_parameters=[configuration])
    def get(self, request):
        loggedUser = get_logged_user(request)
        if loggedUser is not None:
            try:
                configurationId = request.GET['configuration']
                queryset = Delete.objects.filter(group__configuration=configurationId)
            except:
                queryset = Delete.objects.all()
            serializer = DeleteSerializer(queryset, many=True)
            return Response(serializer.data)
        else:
            return Response(status=status.HTTP_401_UNAUTHORIZED)

    @swagger_auto_schema(operation_description="Create an delete/group where you analyze it.")
    def post(self, request, *args, **kwargs):
        loggedUser = get_logged_user(request)
        if loggedUser is not None:
            serializer = DeleteSerializer(data=request.data,partial=True)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            else:
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response(status=status.HTTP_401_UNAUTHORIZED)
