from rest_framework import status
from Server.models import Configuration, Process
from rest_framework import generics
from rest_framework.response import Response
from utils.authentication_utils import get_logged_user
from utils.Assertions import Assertions
from .serializers import *
from django.db import transaction
from drf_yasg.utils import swagger_auto_schema
from drf_yasg import openapi
from rest_framework import permissions


class OCRManager(generics.DestroyAPIView):
    queryset = OCR.objects.all()

    def get_object(self, pk=None):
        if pk is None:
            pk = self.kwargs['pk']
        try:
            return OCR.objects.get(pk=pk)
        except OCR.DoesNotExist:
            Assertions.assert_true_raise(False,404,"Not found")

    @swagger_auto_schema(operation_description="Delete an OCR by id.")
    def delete(self, request, pk=None, format=None):
        loggedUser = get_logged_user(request)
        if loggedUser is not None:
            if pk is None:
                pk = self.kwargs['pk']
            ocr = self.get_object(pk)
            ocr.delete()
            return Response(status=status.HTTP_204_NO_CONTENT)
        else:
            return Response(status=status.HTTP_401_UNAUTHORIZED)

class ListOCR(generics.CreateAPIView):
    serializer_class = OCRSerializer

    def get_queryset(self):
        return OCR.objects.all()

    @swagger_auto_schema(operation_description="Ceate an OCR in the screenshots by configuration.")
    def post(self, request, *args, **kwargs):

        loggedUser = get_logged_user(request)
        if loggedUser is not None:
            serializer = OCRSerializer(data=request.data,partial=True)
            try:
                if serializer.is_valid():
                    ocr = serializer.save()
                    if processing_data_ocr(ocr):
                        return Response(serializer.data, status=status.HTTP_201_CREATED)
                else:
                    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
            except:
                return Response(serializer.data, status=status.HTTP_417_EXPECTATION_FAILED)
        else:
            return Response(status=status.HTTP_401_UNAUTHORIZED)
