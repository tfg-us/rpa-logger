from rest_framework import status
from rest_framework import generics
from rest_framework.response import Response
from .serializers import *
from computer.serializers import *
from utils.authentication_utils import get_logged_user
from django.db import transaction
from drf_yasg.utils import swagger_auto_schema


class EstablishConnection(generics.RetrieveAPIView):
    swagger_schema = None

    @swagger_auto_schema(operation_description="Check computer characteristics for logging it.")
    @transaction.atomic
    def get(self, request):

        computer = Computer.objects.filter(MAC=(request.data['MAC']).upper()).first()
        if computer is not None:
            computer.RAM = request.data['RAM']
            computer.primaryDisk = request.data['primaryDisk']
            computer.machine = request.data['machine']
            computer.processor = request.data['processor']
            computer.core = request.data['core']
            computer.system = request.data['system']
            computer.release = request.data['release']
            computer.version = request.data['version']
            computer.namePC = request.data['namePC']
            computer.userPC = request.data['userPC']

        oldToken = LoggerAuthentication.objects.filter(computer=computer.id).first()
        caducity_token(oldToken)
        if oldToken is None:
            logToken = token_save(request)
        else:
            logToken = oldToken
        if logToken is not None:
            headers = {'x-auth': logToken.token}
            computer = Computer.objects.filter(MAC=(request.data["MAC"]).upper()).first()
            capture = Capture.objects.filter(computer=computer,status=True).first()
            if capture is not None:
                project = capture.project
                serializer = ConnectionProjectSerializer(project)
                serializer.data["captures"]["status"] = capture.status
                serializer.data["captures"]["timestamp"] = capture.timestamp
            else:
                project = Project.objects.filter(computers__in=[computer.id]).order_by('creationMoment').first()
                if project is None:
                    sendPolicy = SendPolicy.objects.filter(default=True).first()
                    capturePolicy = CapturePolicy.objects.filter(default=True).first()
                    project = Project(projectName="TMP",description="Empty project for an unasigned PC.",sendPolicy=sendPolicy,capturePolicy=capturePolicy)
                serializer = ConnectionProjectSerializer(project)
                serializer.data["captures"]["status"] = False
            return Response(serializer.data, status=status.HTTP_200_OK, headers=headers)
        else:
            return Response(status=status.HTTP_401_UNAUTHORIZED)

