from rest_framework import serializers
from Server.models import Module, CapturePolicy
from utils.Assertions import *
from utils.strings import Strings
from capture.serializers import *

class ModuleSerializer(serializers.ModelSerializer):

    class Meta:
        model = Module
        fields = ('id','name','capturePolicy')

    def validate(self, attrs):

            data = attrs

            if data.get("name"):
                Assertions.assert_true_raise(Strings.check_type(data.get('name')),401, 'ERROR_MODULE_NAME')
            return data



class ShortModuleSerializer(serializers.ModelSerializer):

    class Meta:
        model = Module
        fields = ('id','name')