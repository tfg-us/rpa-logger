from rest_framework import serializers
from Server.models import Capture, Computer, Project
from utils.Assertions import *
from utils.strings import Strings
from event.serializers import *
from screenshot.serializers import *
import time


class CaptureStatusSerializer(serializers.ModelSerializer):

    class Meta:
        model = Capture
        fields = ('id','timestamp','status')

class CaptureCreateSerializer(serializers.ModelSerializer):

    class Meta:
        model = Capture
        fields = ('id', 'timestamp', 'project', 'status', 'computer')

    def validate(self, attrs):

            data = attrs

            if data.get("computer"):
                projects = Project.objects.filter(computers=data.get("computer"))
                project = data.get("project")
                value = False
                for i in projects:
                    if isinstance(project, int):
                        if i.id == project:
                            value = True
                            break
                    else:
                        if i == project:
                            value = True
                            break
                Assertions.assert_true_raise(value,400, ('ERROR_COMPUTER'))
            return data

class CaptureConfigurationSerializer(serializers.ModelSerializer):
    screenshots = ScreenshotSerializer(many=True,read_only=True)

    class Meta:
        depth = 1
        model = Capture
        fields = ('id','timestamp','screenshots')


class CaptureSerializer(serializers.ModelSerializer):
    events = EventSerializer(many=True,read_only=True)
    screenshots = ScreenshotSerializer(many=True,read_only=True)

    class Meta:
        depth = 1
        model = Capture
        fields = ('id','timestamp','project','status','computer','events','screenshots')

    def validate(self, attrs):

            data = attrs

            if data.get("timestamp"):
                Assertions.assert_true_raise((data.get('timestamp') <= int(round(time.time() * 1000))),401, ('ERROR_CAPTURE_TIME'))
            return data
