from rest_framework import serializers
from Server.models import Keyboard
from utils.Assertions import *
from utils.strings import Strings
from capture.serializers import *

class KeyboardSerializer(serializers.ModelSerializer):

    class Meta:
        model = Keyboard
        fields = ('id','keywordNumber')

    def validate(self, attrs):

            data = attrs

            if data.get("keywordNumber"):
                Assertions.assert_true_raise(Strings.check_number_max(data.get('keywordNumber'), 100000000000),401, ('ERROR_KEYWORDNUMBER_LENGHT'))
            return data

