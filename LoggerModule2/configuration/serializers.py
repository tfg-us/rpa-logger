from rest_framework import serializers
from Server.models import Configuration, Project, Screenshot, Similarity, Server_configuration
from capture.serializers import CaptureSerializer
from project.serializers import ProjectConfigurationSerializer
from ocr.serializers import OCRSerializer
from note.serializers import NoteSerializer
from similarity.serializers import SimilaritySerializer
from utils.Assertions import *
from utils.strings import Strings
from utils.configuration import Configuration_server
from django.db import transaction
from requests_toolbelt.multipart.encoder import MultipartEncoder
import requests
import json

class ShortConfigurationSerializer(serializers.ModelSerializer):
    notes = NoteSerializer(many=True)

    class Meta:
        model = Configuration
        fields = ('id','umbral','project','notes')

    def validate(self, attrs):

        data = attrs

        if data.get("umbral"):
            Assertions.assert_true_raise(Strings.check_number_max(data.get('umbral'), 1.00), 401, ('ERROR_UMBRAL'))
        return data

    @transaction.atomic
    def save(self, **kwargs):
        data = self.data
        umbral = data["umbral"]
        project = data["project"]
        project = Project.objects.filter(pk=project).first()
        if project is not None:
            try:
                pk = data["id"]
            except:
                pk= None
            if pk is None:
                configuration = Configuration(umbral=umbral, project=project)
            else:
                configuration = Configuration(pk=pk, umbral=umbral, project=project)
            configuration.save()
            notes = data["notes"]
            if notes is not None:
                for note in notes:
                    note["configuration"]=int(configuration.id)
                    try:
                        pk_no = note.id
                    except:
                        pk_no = None
                    if pk_no is None:
                        serializer = NoteSerializer(data=note, partial=True)
                        serializer.configuration=configuration
                    else:
                        noteBD = self.get_object(note.id)
                        serializer = NoteSerializer(noteBD, data=note, partial=True)
                    if serializer.validate(note):
                        serializer.is_valid()
                        serializer.save()
        return configuration

class ConfigurationGroupSerializer(serializers.ModelSerializer):
    project = ProjectConfigurationSerializer(many=False,read_only=True)

    class Meta:
        depht = 1
        model = Configuration
        fields = ('id','umbral','project')

class ConfigurationSerializer(serializers.ModelSerializer):
    OCRS = OCRSerializer(many=True)
    notes = NoteSerializer(many=True)
    similarities = SimilaritySerializer(many=True)

    class Meta:
        depht = 1
        model = Configuration
        fields = ('id','umbral','project','OCRS','similarities','notes')

def call_processing(configuration):
    try:
        server = str((Server_configuration.objects.all().first()).serverProcessing)+"/fingerPrint/"
        serialized = ConfigurationGroupSerializer(configuration)
        jsonFile = json.dumps(serialized.data)
        header = {'Server-token':(Server_configuration.objects.all().first()).serverAPIToken}
        result = requests.post(server, json=jsonFile,headers=header)
        if result.status_code == 201:
            return result
        else:
            return None
    except:
        return None
