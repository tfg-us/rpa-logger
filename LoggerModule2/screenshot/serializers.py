from rest_framework import serializers
from Server.models import Screenshot
from utils.Assertions import *
from utils.strings import Strings
from capture.serializers import *
from LoggerModule2.settings import *
import re
from django.db import models
from pathlib import Path

class ScreenshotSerializer(serializers.ModelSerializer):

    class Meta:
        model = Screenshot
        fields = ('id', 'imagePath', 'timestamp', 'capture')

    def validate(self, attrs):

            data = attrs

            if data.get("imagePath"):
                img_size = len(data.get("imagePath"))
                Assertions.assert_true_raise(img_size <= 2097152,401, 'ERROR_IMAGE_LENGHT')
            if data.get("timestamp"):
                Assertions.assert_true_raise((int(round((time.time() * 1000)) > int(data.get("timestamp"))), 119), 401,('ERROR_TIMESTAMP'))
            return data

