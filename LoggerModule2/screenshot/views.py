from rest_framework import status
from Server.models import Project, Screenshot, Computer, Capture, Similarity
from rest_framework import generics
from rest_framework.response import Response
from utils.authentication_utils import get_token_computer
from utils.Assertions import Assertions
from .serializers import *
from django.http import HttpResponse
from requests_toolbelt import MultipartEncoder
from utils.authentication_utils import get_logged_user,get_server_token_access
from drf_yasg.utils import swagger_auto_schema

class DownloadScreenshotImage(generics.RetrieveAPIView):
    queryset = Screenshot.objects.all()
    serializer_class = ScreenshotSerializer

    @swagger_auto_schema(operation_description="Download a screenshot image by image name.")
    def get(self, request, filename=None, format=None):
        loggedUser = get_logged_user(request)
        if loggedUser is not None:
            if filename is None:
                filename = self.kwargs['pk']
            screenshot = Screenshot.objects.filter(imagePath="screenshots/capture/"+filename).first()
            if screenshot is not None:
                filename = screenshot.imagePath.file.name.split('\\')[-1]
                response = HttpResponse(screenshot.imagePath.file, content_type='text/plain')
                response['Content-Disposition'] = 'attachment; filename=%s' % filename
            else:
                response = Response(status=status.HTTP_404_NOT_FOUND)
            return response
        else:
            return Response(status=status.HTTP_401_UNAUTHORIZED)

class ScreenshotManager(generics.RetrieveAPIView):
    queryset = Screenshot.objects.all()
    serializer_class = ScreenshotSerializer
    swagger_schema = None

    def get_object(self, pk=None):
        if pk is None:
            pk = self.kwargs['pk']
        try:
            return Screenshot.objects.get(pk=pk)
        except Screenshot.DoesNotExist:
            Assertions.assert_true_raise(False,404,"Not found")

    @swagger_auto_schema(operation_description="Download a screenshot image by id. This is used for the processing server.")
    def get(self, request, pk=None, format=None):
        processingServer = get_server_token_access(request, "PROCESSING")
        if processingServer is not None:
            if pk is None:
                pk = self.kwargs['pk']
            screenshot = self.get_object(pk)
            filename = screenshot.imagePath.file.name.split('\\')[-1]
            response = HttpResponse(screenshot.imagePath.file, content_type='text/plain')
            response['Content-Disposition'] = 'attachment; filename=%s' % filename
            return response
        else:
            return Response(status=status.HTTP_401_UNAUTHORIZED)


class CreateEventScreenshot(generics.CreateAPIView):
    serializer_class = ScreenshotSerializer
    swagger_schema = None

    def get_queryset(self):
        return Screenshot.objects.all()

    @swagger_auto_schema(operation_description="Create a screenshot by the logger in the computer.")
    def post(self, request, *args, **kwargs):
        loggedComputer = get_token_computer(request)
        if loggedComputer is not None:
            capture = Capture.objects.filter(computer=loggedComputer,saveScreenshot=False).first()
            if capture is not None:
                data={}
                data["capture"] = capture.id
                data["imagePath"] = request.data["imagePath"]
                data["timestamp"] = int(request.data['timestamp'])
                rest = request.data['rest']
                serializer = ScreenshotSerializer(data=data)
                if serializer.validate(request.data):
                    serializer.is_valid()
                    serializer.save()
                if not capture.status and rest == 'True':
                    capture.saveScreenshot = True
                capture.save()
                return Response(status=status.HTTP_201_CREATED)
            else:
                Assertions.assert_true_raise(False, 404, "Capture not found")
        else:
            return Response(status=status.HTTP_401_UNAUTHORIZED)
