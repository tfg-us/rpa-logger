from rest_framework import generics
from Server.models import Group
from .serializers import *
from utils.Assertions import *
from rest_framework.response import Response
from rest_framework import status
import requests
from utils.authentication_utils import get_logged_user, get_server_token_access
from drf_yasg.utils import swagger_auto_schema
from drf_yasg import openapi

class GroupManager(generics.RetrieveAPIView):
    queryset = Group.objects.all()
    serializer_class = GroupSerializer

    def get_object(self, pk=None):
        if pk is None:
            pk = self.kwargs['pk']
        try:
            return Group.objects.get(pk=pk)
        except Group.DoesNotExist:
            Assertions.assert_true_raise(False,404,"Not found")

    @swagger_auto_schema(operation_description="Get a group by id.")
    def get(self, request, pk=None, format=None):
        loggerUser = get_logged_user(request)
        serverRPA = get_server_token_access(request,"RPA")
        if loggerUser is not None or serverRPA is not None:
            if pk is None:
                pk = self.kwargs['pk']
            group = self.get_object(pk)
            serializer = GroupSerializer(group)
            return Response(serializer.data)
        else:
            return Response(status=status.HTTP_401_UNAUTHORIZED)

class ListGroup(generics.ListCreateAPIView):
    serializer_class = GroupCreateSerializer

    def get_queryset(self):
        return Group.objects.all()

    configuration = openapi.Parameter('configuration', openapi.IN_QUERY, description="List by configuration id.", type=openapi.TYPE_INTEGER)

    @swagger_auto_schema(operation_description="List groups. You must list groups by configuration id.",manual_parameters=[configuration])
    def get(self, request):
        loggerUser = get_logged_user(request)
        serverRPA = get_server_token_access(request,"RPA")
        if loggerUser is not None or serverRPA is not None:
            try:
                configurationId = request.GET['configuration']
                queryset = Group.objects.filter(configuration=configurationId,deleteGroup__isnull=True,groupDestination__isnull=True)
                serializer = GroupCreateSerializer(queryset, many=True)
                return Response(serializer.data)
            except:
                return Response(status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response(status=status.HTTP_401_UNAUTHORIZED)

    @swagger_auto_schema(auto_schema=None,operation_description="The server processing create groups by finger print when you create a configuration.",responses={401: "Unauthorized for user"})
    def post(self, request, *args, **kwargs):
        serverProcessing = get_server_token_access(request,"PROCESSING")
        if serverProcessing is not None:
            data = prepare_data_to_save(request)
            serializer = GroupCreateSerializer(data=data,many=True)
            if serializer.is_valid():
                group = saveAuto(data)
                serialized = GroupCreateSerializer(group)
                return Response(serialized.data, status=status.HTTP_201_CREATED)
        else:
            return Response(status=status.HTTP_401_UNAUTHORIZED)

