from rest_framework import serializers
from Server.models import Group, Configuration, Screenshot, Event
from delete.serializers import *
from equal.serializers import *
from django.db import transaction

class GroupSerializer(serializers.ModelSerializer):
    deleteGroup = DeleteSerializer()
    groupOrigin = EqualSerializer()

    class Meta:
        depth = 1
        model = Group
        fields = ('id','screenshots','events','configuration','deleteGroup','groupOrigin')


class GroupCreateSerializer(serializers.ModelSerializer):

    class Meta:
        depth =1
        model = Group
        fields = ('id','screenshots','configuration')

'''
Save groups by the processing server
'''
@transaction.atomic
def saveAuto(data):
    listDict = []
    for i in data:
        group = Group(configuration=i['configuration'])
        group.save()
        for a in i['screenshots']:
            events = Event.objects.filter(capture=a.capture)
            for b in events:
                if b.timestamp == a.timestamp:
                    group.events.add(b)
            group.screenshots.add(a)
        listDict.append(group)
    return listDict

'''
Change the format of data for storage purpose
'''
def prepare_data_to_save(request):
    data = request.data

    if data is not None:
        configurationId = data[0]["configuration"]
        configuration = Configuration.objects.filter(pk=configurationId).first()
        listData = []
        for i in data:
            dict = {}
            dict['configuration'] = configuration
            screenshots = []
            for scre in i['screenshots']:
                pk = scre['id']
                screenshot = Screenshot.objects.filter(pk=pk).first()
                screenshots.append(screenshot)
            dict['screenshots']=screenshots
            listData.append(dict)
        return listData