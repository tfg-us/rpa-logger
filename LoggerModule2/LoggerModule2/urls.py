"""LoggerModule2 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import url, include
from rest_framework import routers
from computer.views import *
from project.views import *
from event.views import *
from login.views import *
from capture.views import *
from screenshot.views import *
from sendPolicy.views import *
from capturePolicy.views import *
from connection.views import *
from process.views import *
from group.views import *
from delete.views import *
from equal.views import *
from configuration.views import *
from similarity.views import *
from ocr.views import *
from note.views import *
from rest_framework.schemas import get_schema_view
from drf_yasg.views import get_schema_view
from drf_yasg import openapi
from rest_framework import permissions

router = routers.DefaultRouter()

schema_view = get_schema_view(
   openapi.Info(
      title="API for LoggerModule2",
      default_version='v1',
      description="These endpoints are for the actions performed by the system's logger administrator. There are more that are not available for unauthorized access as they are only for the logger tool and the processing server.",
      terms_of_service="https://www.google.com/policies/terms/",
      contact=openapi.Contact(email="joslopcar@alum.us.es"),
      license=openapi.License(name="BSD License"),
   ),
   public=True,
   permission_classes=(permissions.AllowAny,),
)
urlpatterns = [
    url(r'^api/doc(?P<format>\.json|\.yaml)$', schema_view.without_ui(cache_timeout=0), name='schema-json'),
    url(r'^api/doc/$', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    path('admin/', admin.site.urls),
    path('',include(router.urls)),
    url(r'^login/$', LoginManager.as_view(), name='login'),
    url(r'^connection/$', EstablishConnection.as_view()),
    url(r'^events/$', CreateEvent.as_view()),
    url(r'^computers/$', ListComputers.as_view()),
    url(r'^computers/(?P<pk>[0-9]+)/$', ComputerManager.as_view()),
    url(r'^projects/$', ListProject.as_view()),
    url(r'^projects/(?P<pk>[0-9]+)/$', ProjectManager.as_view()),
    url(r'^captures/(?P<pk>[0-9]+)/$', CaptureManager.as_view()),
    url(r'^captures/$',CreateCapture.as_view()),
    url(r'^sendPolicies/$', ListSendPolicy.as_view()),
    url(r'^sendPolicies/(?P<pk>[0-9]+)/$', SendPolicyManager.as_view()),
    url(r'^capturePolicies/$', ListCapturePolicy.as_view()),
    url(r'^capturePolicies/(?P<pk>[0-9]+)/$', CapturePolicyManager.as_view()),
    url(r'^configurations/$', ListConfiguration.as_view()),
    url(r'^configurations/(?P<pk>[0-9]+)/$', ConfigurationManager.as_view()),
    url(r'^ocrs/$', ListOCR.as_view()),
    url(r'^ocrs/(?P<pk>[0-9]+)/$', OCRManager.as_view()),
    url(r'^similarities/$', ListSimilarity.as_view()),
    url(r'^similarities/(?P<pk>[0-9]+)/$', SimilarityManager.as_view()),
    url(r'^notes/$', ListNote.as_view()),
    url(r'^notes/(?P<pk>[0-9]+)/$', NoteManager.as_view()),
    url(r'^delete/groups/$', ListDelete.as_view()),
    url(r'^delete/groups/(?P<pk>[0-9]+)/$', DeleteManager.as_view()),
    url(r'^equal/groups/$', ListEqual.as_view()),
    url(r'^equal/groups/(?P<pk>[0-9]+)/$', EqualManager.as_view()),
    url(r'^groups/$', ListGroup.as_view()),
    url(r'^groups/(?P<pk>[0-9]+)/$', GroupManager.as_view()),
    url(r'^process/$', ProcessManager.as_view()),
    url(r'^screenshots/(?P<pk>[0-9]+)/$', ScreenshotManager.as_view()),
    url(r'^screenshots/$', CreateEventScreenshot.as_view()),
    url(r'^media/screenshots/capture/(?P<filename>[A-Za-z0-9-._]*)/$', DownloadScreenshotImage.as_view()),
    url(r'^media/configuration/similarities/(?P<filename>[A-Za-z0-9-._]*)/$', DownloadSimilarityImage.as_view())
]
