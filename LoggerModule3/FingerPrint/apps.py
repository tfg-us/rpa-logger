from django.apps import AppConfig

class FingerPrint(AppConfig):
    name = 'fingerPrints'