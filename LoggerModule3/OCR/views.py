from rest_framework import status
from rest_framework import generics
from rest_framework.response import Response
from utils.Assertions import Assertions
from .serializers import *
from utils.utils import *
from LoggerModule3.settings import SERVER_PROCESSING_TOKEN

'''
Returns whether an image contains text with a true or false
'''
class OCRManager(generics.CreateAPIView):
    serializer_class = OCRSerializer

    def post(self, request, pk=None, format=None):
        if get_server_validation(request):
            valor = ocr_process(request)
            header = {'Server-token': SERVER_PROCESSING_TOKEN}
            if valor is not None:
                return Response(valor,status=status.HTTP_200_OK,headers=header)
            else:
                return Response(status=status.HTTP_400_BAD_REQUEST,headers=header)
        else:
            return Assertions.assert_true_raise(False,404,"Server not valid")
