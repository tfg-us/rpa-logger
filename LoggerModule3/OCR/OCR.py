# -*- coding: utf-8 -*-
import pytesseract as ts
import cv2 as cv
import numpy as np
import os
from PIL import Image
from LoggerModule3.settings import TESSERACT_PATH

if TESSERACT_PATH != '':
    ts.pytesseract.tesseract_cmd = TESSERACT_PATH

'''
We get with Tesseract all the text of an image and then we check if the text 
passed by the user is in that generated text
'''
def optical_character_recognition(textOCR,img):
    value = False
    img = cv.imdecode(np.frombuffer(img, np.uint8), 1)
    gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
    gray = cv.threshold(gray, 0, 255,cv.THRESH_BINARY | cv.THRESH_OTSU)[1]
    text = ts.image_to_string(gray,lang='eng')
    if textOCR in text:
        value = True
    return value

