from LoggerModule3.settings import SERVER_API_TOKEN

def get_server_validation(request):
    try:
        value = request.META['HTTP_SERVER_TOKEN']
        if str(value)==SERVER_API_TOKEN:
            return True
        else:
            return None
    except:
        return None