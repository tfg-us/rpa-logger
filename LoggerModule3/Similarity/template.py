import cv2 as cv
import numpy as np
import imutils

'''
We compare the template image with the main image to see if it is contained in it.
The function automatically returns false if the template image is larger than the image you are looking for.
'''
def template_mathcing(img,template):
    umbral = 0.8
    img = cv.imdecode(np.frombuffer(img, np.uint8), 1)
    template = cv.imdecode(np.frombuffer(template, np.uint8), 1)
    template = cv.cvtColor(template, cv.COLOR_BGR2GRAY)
    template = cv.Canny(template, 50, 200)
    h, w = template.shape[:2]
    gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
    value = None
    for scale in np.linspace(0.2, 1.0, 20)[::-1]:
        resized = imutils.resize(gray, width=int(gray.shape[1] * scale))
        r = gray.shape[1] / float(resized.shape[1])
        if resized.shape[0] < h or resized.shape[1] < w:
            break
        edged = cv.Canny(resized, 50, 200)
        result = cv.matchTemplate(edged, template, cv.TM_CCORR_NORMED)
        (_, maxVal, _, maxLoc) = cv.minMaxLoc(result)
        if value is None or maxVal > value[0]:
            value = (maxVal, maxLoc, r)
    if value is not None:
        if value[0]>umbral:
            return True
        else:
            return False
    else:
        return False

