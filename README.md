# Logger for RPA purposes

Welcome to the Open Source Logger. 

This project is for the creation of a platform for the registration of information and its easy storage, giving priority to ease and simplicity over other features that this project lacks and that can be extended in the future.

## Index

1. Introduction

2. Previus installation

3. Previus configuration

4. Execution

5. How use it

6. FAQs

## 1. Introduction
This logger will help institutions and developers to obtain user activity files that can be used for artificial intelligence training or, as indicated in the header, the creation of RPA ( Robotic Process Automation).  
This project arises from the need to obtain information and data that can be used by other tools and not only for statistical purposes or equipment monitoring as is usually found in the current market.
The project has the logger application installed on the host computer as **Logger-Module1**, a server that will collect and store this information in a database for easy access, without having to read from files as **LoggerModule2**, and an information processing server to extract additional information from the activity as **LoggerModule3**.
The processing server makes use of OCR, template mathcing and finger print as information processing methods, but they only have a basic implementation as examples to be used by LoggerModule2 on demand.

## 2. Previus installation

Please read the entire page before starting the process.

To start you need to install python, (3.7.4) on your computer as indicated in the official documentation[link to python!](https://www.python.org/downloads/)

To configure the project, it is necessary to install the dependencies of each module.

Each module has its own dependencies, which can be installed from the requirements.txt file in the root directory of each module:

`pip install -r requirements.txt`

### Logger-Module1

Logger(Logger-Module1) has executable program for Windows that do not need to be pre-configured in the *executable* directory. Double click on the program to run it without the *requirements.txt*.

If you want to use the code or run it in Linux, install the dependencies of the *requirements.txt*. And execute:

In case you run on:

windows

`pip install pywin32`

Linux

`sudo apt install python3-gi`

`sudo apt-get install scrot`

### LoggerModule2

The LoggerModule2 uses a data base. We recommend PostgreSQL[link to postgresql!](https://www.postgresql.org/download/).

Execute in a terminal if ypu have problems in the execution section:

`pip install libpq-dev`

`pip install psycopg2`


### LoggerModule3

The LoggerModule3 needs tesseract to perform OCR (Optical Character Recognition) to the screenshots. Install it in the same machine that has LoggerModule3 [link to tesseract!](https://github.com/tesseract-ocr/tesseract/wiki)


## 3. Previus configuration

### Logger-Module1

For Logger(Logger-Module1) there must be a *configuration-properties.json* file with the basic configuration of the project.

If the file doesn't exist, you must create a file with this name the same directory that the executable or in the configuration folder within the Logger-Module1 project. The content must be:

{"server":"<https://example.com>","projectName":""}

The "server" must be the LoggerModule2 server.

### LoggerModule2

You must execute the following commands on a terminal with the changes made:

Open the postgresql terminal:

`psql -c "create user <DataBase_User_Name> with password '<DataBase_User_Password>'"`

`psql -c "create database <Database_name> owner <DataBase_User_Name>"`

Before starting LoggerModule2, you will need to edit the *settings.py* file located in the LoggerModule2 directory within the project. 

Generate a SECRET_KEY to replace the one published in git.

If you deploy in production:

`DEBUG = False`

In the section DATABASES replace with the one you configured in postgreSQL and in first part of this section:

- NAME
- HOST
- PORT
- USER
- PASSWORD
- TEST


### LoggerModule3

Before starting LoggerModule3, you will need to edit the *settings.py* file located in the LoggerModule3 directory within the project. 

Generate a SECRET_KEY to replace the one published in git.

If you deploy in production:

`DEBUG = False`

Modify parameters:

`TESSERACT_PATH = '<https://example.com>'`

`SERVER_API = '<https://example.com>'`

`SERVER_API_TOKEN = '<Secure_token_API>'`

`SERVER_PROCESSING_TOKEN = '<Secure_token_Processing>'`

Where *TESSERACT_PATH* is the path of the tesseract executable on the machine.
Where the *SERVER_API* will be the server you will use to store the information.
Where the *SERVER_API_TOKEN* will be a secure token that will be issued for security checks, it has to match the one stored in the *LoggerModule2* as you will see in the *4. Execution*.
Where the *SERVER_PROCESSING_TOKEN* will be a secure token that will be issued for security checks, it has to match the one stored in the *LoggerModule2* as you will see in the *4. Execution*.


## 4. Execution

### Logger-Module1

In case you want to use the code and not an executable, run in a terminal in the root of the project:

`python3 main.py`

### LoggerModule2

In the root of the project, where the *manage.py* file is located, open a terminal and run with the modifications:

`python3 manage.py makemigrations <Database_name>`

`python3 manage.py createsuperuser`

`python3 manage.py runserver <Default_Port>`

When you create a super user, fill in the information as you wish. The super user will be an administrator who will have access to the database at https://example.com/admin/. 

There is a set of data that you will have to configure manually in the *Server_configuration* table. To do this, go to https://example.com/admin/ and in the present configuration table, edit the values and add:

- serverAPI: with the URL of the server where the *LoggerModule2* is located.
- serverProcessing = with the URL of the server where the *LoggerModule3* is located.
- serverRPA = with the URL of the server where the RPA learning or automation system is located.
- serverFrontend = with the URL of the server where the frontend server is located for use by the system administrator in his control of the information collected in *LoggerModule2*.
- serverAPIToken = Secret token that must not be published, for the secure connection between *LoggerModule2* and *LoggerModule3*.
- serverProcessingToken = Secret token that must not be published, for the secure connection between *LoggerModule2* and *LoggerModule3*.
- serverRPAToken = Secret token that must not be published, for the secure connection between *LoggerModule2* and the RPA learning or automation system.


### LoggerModule3

In the root of the project, where the *manage.py* file is located, open a terminal and run with the modifications:

`python3 manage.py runserver <Default_Port>`

## 5. How use it

For Logger-Module1 only need start it. But previusly you need register the computer in the system.

For LoggerModule2, access to:
- /admin/: to the user administrator and the Server-Configuration. You are logged in with the previously created superuser. New users are registered from the user section.
- /api/doc/: to the endpoints of the projects. The endpoint login is necessary is to get an authorization token that you will have to insert at the top of the swagger screen in 'Authorization'.

The LoggerModule3 is used by LoggerModule2. The endpoints are:
- /ocr/
- /similarity/
- /fingerPrint/
These endpoints can't be used without the tokenAPIServer in the header 'Server-token'.

## 6. FAQs

- If you have any problems, try to run the commands as an admin. 

- In the LoggerModule 3. If you had problems in Windows with Tesseract, change the TESSERACT_PATH in the settings.py with the executable path.