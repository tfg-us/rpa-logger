# !/usr/bin/env python
import logging
from configuration.configuration import modules_policy
"""
The function active_windows is from: https://stackoverflow.com/questions/10266281/obtain-active-window-using-python
"""
def active_window():
    if not modules_policy("FOREGROUNDAPP"):
        """
        Get the currently active window.
    
        Returns
        -------
        string :
            Name of the currently active window.
        """
        import sys
        import os
        import subprocess
        import re
        name = None
        if sys.platform in ['Linux','linux', 'linux2']:
            # https://askubuntu.com/questions/1199306/python-get-foreground-application-name-in-ubuntu-19-10
            root = subprocess.Popen(['xprop', '-root', '_NET_ACTIVE_WINDOW'], stdout=subprocess.PIPE)
            stdout, stderr = root.communicate()
            m = re.search(b'^_NET_ACTIVE_WINDOW.* ([\w]+)$', stdout)
            if m != None:
                window_id = m.group(1)
                window = subprocess.Popen(['xprop', '-id', window_id, 'WM_CLASS'], stdout=subprocess.PIPE)
                stdout, stderr = window.communicate()
            else:
                return "unknown"

            match = re.match(b'WM_CLASS\(\w+\) = ".*", (?P<name>.+)$', stdout)
            if match != None:
                name = (match.group("name").strip(b'"')).decode("utf-8")
        elif sys.platform in ['Windows', 'win32', 'cygwin']:
            # http://stackoverflow.com/a/608814/562769
            import win32gui
            window = win32gui.GetForegroundWindow()
            name = win32gui.GetWindowText(window)
        elif sys.platform in ['Mac', 'darwin', 'os2', 'os2emx']:
            # http://stackoverflow.com/a/373310/562769
            from AppKit import NSWorkspace
            name = (NSWorkspace.sharedWorkspace()
                .activeApplication()['NSApplicationName'])
        else:
            print("sys.platform={platform} is unknown. Please report."
                  .format(platform=sys.platform))
            print(sys.version)
        return name

