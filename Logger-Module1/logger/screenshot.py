import pyautogui
from configuration.configuration import actions_policy, modules_policy, project_name, capture_name, json_file_concat_list
from pathlib import Path
import tkinter as tk
import threading
import os
import time
from logger.foregrounapp import active_window

json_file_imagen = []
keyNumber_count = 0
firstAPP = ""
characters = ""
stop_thread_image=False

'''
Use the capture policy
'''
def actions_rules(key):
    global keyNumber_count
    global characters
    global firstAPP
    captureStatus = False
    if key is not None:
        try:
            keyNumber_count = keyNumber_count+1
            characters = characters + key
            action = actions_policy("image")
            if action is not None:
                keyNumber = action["keyNumber"]
                triggerKey = action["triggerKey"]
                changeAPP = action["changeApp"]
                tamT = len(triggerKey)
                tamK = len(characters)
                secondAPP = active_window()
                characters = characters+key
                subStr = characters[tamK-tamT-1:]
                if triggerKey is not None and triggerKey is not "" and subStr == triggerKey:
                    captureStatus=True
                if keyNumber_count >= keyNumber or keyNumber==0:
                    captureStatus=True
                    keyNumber_count = 0
                if changeAPP and firstAPP is not secondAPP:
                    captureStatus=True
                firstAPP = active_window()
                return captureStatus
            else:
                return captureStatus
        except:
            return captureStatus
    else:
        return True


"""
If a key or mouse event had captured, the imagen is created
"""
def capture_image(moment,key):
    if not modules_policy("SCREENSHOT"):
        if actions_rules(key):
            projectName = project_name()
            captureName = capture_name()
            path = os.getcwd()
            Path(path+"/captures/"+projectName+"/"+captureName).mkdir(parents=True, exist_ok=True)
            img = pyautogui.screenshot()
            name = str(moment)
            newPath = path+"/captures/"+projectName+"/"+captureName+"/"+name+".png"
            img.save(newPath)
            resolution = screen_resolution()
            values = [newPath,resolution,moment]
            write_json_imagen(values)
'''
Detect the screen size
'''
def screen_resolution():
    inter = tk.Tk()
    inter.withdraw()
    x= inter.winfo_screenheight()
    y = inter.winfo_screenwidth()
    resolution = (y,x)
    return resolution

'''
Generate a json content
'''
def write_json_imagen(values):
    global json_file_imagen
    json_file = {
        "imagePath": str(values[0]),
        "size": values[1],
        "timestamp":  values[2]
    }
    json_file_imagen.append(json_file)
    return json_file

'''
Start the screenshot thread that takes a screenshot over a period of time
'''
def start_image_timer_period():
    global stop_thread_image
    actions = actions_policy("image")
    if actions is not None:
        period = actions["image"]["periodTime"]
    else:
        period = 0
    if not modules_policy("SCREENSHOT") and actions is not None and not stop_thread_image:
        if period is not 0:
            moment = int(round(time.time() * 1000))
            capture_image(moment, None)
            screenTh  = threading.Timer(period,start_image_timer_period).start()
        else:
            return None
    else:
        return None

'''
Start the screenshot thread that detect the inactiveTime
'''
def start_image_timer_inactive():
    global stop_thread_image
    actions = actions_policy("image")
    if actions is not None:
        inactive = actions["image"]["inactiveTime"]
    else:
        inactive = 0
    if not modules_policy("SCREENSHOT") and actions is not None and inactive !=0 and not stop_thread_image:
        capture_inactive()
        screenTh  = threading.Timer(1,start_image_timer_inactive).start()
    else:
        return None

'''
Checks the period of inactivity to perform the capture
'''
def capture_inactive():
    global stop_thread_image
    action_policy = actions_policy("image")
    if action_policy is not None:
        inactive = action_policy["image"]["inactiveTime"]
    else:
        inactive = 0
    if not modules_policy("SCREENSHOT") and action_policy is not None and inactive !=0:
        json_concat = json_file_concat_list()+json_file_imagen_list()
        if json_concat:
            actions = sorted(json_concat, key=lambda i: i['timestamp'])
            moment_old = actions[len(actions)-1]['timestamp']
            moment_new = int(round(time.time() * 1000))
            if moment_new - moment_old >=inactive*1000:
                moment = int(round(time.time() * 1000))
                capture_image(moment, None)
    else:
        return None

'''
Stop the screenshot thread
'''
def stop_image():
    global stop_thread_image
    stop_thread_image = True

'''
Return the image list json
'''
def json_file_imagen_list():
    global json_file_imagen
    return json_file_imagen

'''
Reset the image list json by a value
'''
def json_file_imagen_list_reset(value):
    global json_file_imagen
    if value is None:
        json_file_imagen = []
    else:
        try:
            json_file_imagen = json_file_imagen[value:]
        except:
            return json_file_imagen
    return json_file_imagen
