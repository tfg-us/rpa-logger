import pyperclip
from configuration.configuration import modules_policy, json_file_concat_add
import time
import threading
from tkinter import Tk

stop_thread = False

"""
Basic function to read the paperclip content
"""
old_value = ""

def copy_paperclip():
    global old_value
    if not modules_policy("CLIPBOARD"):
        r = Tk()
        r.withdraw()
        try:
            new_value = r.clipboard_get()
        except:
            new_value = ""
        if old_value != new_value:
            old_value = new_value
            moment = int(round(time.time() * 1000))
            write_to_json([new_value, moment])

"""
Write capture in json
"""
def write_to_json(values):
    json_file = {
        "nameAPP" : "",
        "type": "CLIPBOARD",
        "coordinate": None,
        "duration": None,
        "content":values[0],
        "clipboard": values[0],
        "size": None,
        "timestamp":  values[1]
    }
    json_file_concat_add(json_file)
    return json_file

"""
Initialize thread for clipboard capture function
"""
def control_clipboard():
    global stop_thread
    stop_thread = False
    thread = threading.Thread(target =copy_paperclip)
    thread.name = "clipboard"
    thread.start()
    return thread

"""
Stop thread for clipboard capture function
"""
def stop_clipboard(th):
    global stop_thread
    stop_thread = True
    if th.is_alive():
        th.join()

