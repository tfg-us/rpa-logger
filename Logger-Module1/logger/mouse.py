from pynput import mouse
from configuration.configuration import json_file_concat_add, modules_policy, actions_policy
import time
from logger.foregrounapp import *
from logger.screenshot import screen_resolution,capture_image
from logger.clipboard import *

start_press_mouse = 0.0
stop_thread_mouse = ""
keyNumber_count = 0

'''
Use the capture policy
'''
def actions_rules(values,type):
    global keyNumber_count
    try:
        keyNumber_count = keyNumber_count+1
        action = actions_policy("mouse")
        if action is not None:
            keyNumber =action["keyNumber"]
            triggerKey = action["triggerKey"]
            if triggerKey is not None and triggerKey is not "" and values[1] == triggerKey:
                if type == "click":
                    write_to_json_click(values)
                elif type == "scroll":
                    write_to_json_scroll(values)
            if keyNumber_count >= keyNumber or keyNumber==0:
                if type == "click":
                    write_to_json_click(values)
                elif type == "scroll":
                    write_to_json_scroll(values)
                keyNumber_count = 0
    except:
        if type == "click":
            write_to_json_click(values)
        elif type == "scroll":
            write_to_json_scroll(values)

'''
Check the mous cick content
'''
def click(x, y, button, pressed):
    global start_press_mouse
    global stop_thread_mouse

    if button != 0:
        """
        In the project documentation, we need the press duration
        We use the 'press' to calculate the time to press and release the button
        and get the duration with the difference
        """
        if pressed:
            start_press_mouse = time.perf_counter()*1000
        else:
            if not pressed:
                duration_new = round((time.perf_counter()*1000) - start_press_mouse,4)
                moment = int(round(time.time() * 1000))
                app = active_window()
                size = screen_resolution()
                values = [(x, y), button, duration_new,moment,app,size]
                capture_image(moment,str(button))
                copy_paperclip()
                actions_rules(values,"click")
    if stop_thread_mouse:
        return False

'''
Check the mouse scroll content
'''
def scroll(x,y,dx,dy):
    global stop_thread_mouse
    shift = False #TODO: Call the function created in the keylog
    direction_y = None
    direction_x = None
    if shift:
        if dy < 0:
            direction_x = 'right'
        else:
            direction_x = 'left'
    else:
        if dy < 0:
            direction_y = 'down'
        else:
            direction_y = 'up'
    """
    value(direction_y,direction_x,mouse_position)
    """
    moment = int(round(time.time() * 1000))
    app = active_window()
    size = screen_resolution()
    value = [direction_y, direction_x , (x, y),moment,app, size]
    capture_image(moment,direction_y)
    actions_rules(value,"scroll")
    if stop_thread_mouse:
        return False


def write_to_json_click(values):
    json_file = {
        "nameAPP" : values[4],
        "type": "MOUSE",
        "coordinate": values[0],
        "duration": values[2],
        "content":str(values[1]),
        "clipboard": "",
        "size": values[5],
        "timestamp":  values[3]
    }
    json_file_concat_add(json_file)
    return json_file

'''
Generate a json content
'''
def write_to_json_scroll(values):
    content = ""
    if values[0] == None and values[1] != None:
        content = values[1]
    elif values[0] != None and values[1] == None:
        content = values[0]
    json_file = {
        "nameAPP" : values[4],
        "type": "MOUSE",
        "coordinate": values[2],
        "duration": None,
        "content":content,
        "clipboard": "",
        "size": values[5],
        "timestamp":  values[3]
    }
    json_file_concat_add(json_file)
    return json_file

'''
Start the mouse thread
'''
def control_mouse_detection():
    global stop_thread_mouse
    if not modules_policy("MOUSE"):
        stop_thread_mouse = False
        mous = mouse.Listener(on_click = click, on_scroll = scroll)
        mous.name = "mouse"
        mous.start()
        return mouse
    else:
        return None

'''
Stop the mouse thread
'''
def stop_mouse_detection(mous):
    global stop_thread_mouse
    if mous is not None:
        try:
            stop_thread_mouse = True
            mous.Listener.stop
        except:
            pass
