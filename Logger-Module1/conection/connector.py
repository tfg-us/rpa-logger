from configuration.configuration import server_url, project_name, capture_name,json_file_concat_list_reset, send_policy
from logger.specificationsPC import *
import requests
import json
import os.path as path
import os
from requests_toolbelt.multipart.encoder import MultipartEncoder
from logger.keylog import *
from logger.mouse import *
from logger.clipboard import *
from logger.screenshot import *
import ast

token = ""
stop_thread_connection = False
statusConnection = False

'''
Connect to the server and get the status of the capture and policies
'''
def connection(test_connection=False):
    global token
    try:
        server = server_url()+"connection/"
        json_file = get_json()
        json_data = json.loads(json_file)
        result = requests.get(server, json=json_data)
        if result.status_code == 200:
            response_json = result.json()
            serverUrl = server_url()
            response_json["server"] = serverUrl
            pathA = os.getcwd()
            file = open(pathA+"/configuration/configuration_properties.json", "w")
            json_respond = json.dumps(response_json)
            file.write(json_respond)
            file.close()
            status = response_json["captures"]["status"]
            token = result.headers["x-auth"]
            if not test_connection:
                post_events(None)
                post_screenshot(None)
            else:
                global statusConnection
                statusConnection = status
            return True,status
        else:
            return False,False
    except:
        return False, False

'''
Post events to the server API
'''
def post_events(jsonEvents):
    global token
    try:
        server = server_url()+"events/"
        projectName = project_name()
        captureName = capture_name()
        pathA = os.getcwd()
        if jsonEvents is None:
            try:
                f = open(pathA+"/captures/" + projectName +"/"+captureName+"/Events.json", "r")
                lines_events = f.readlines()
                f.close()
                jsonEvents = []
                for lines in lines_events:
                    new_line = ast.literal_eval(lines.strip())
                    jsonEvents += new_line
            except:
                return False
        json_data = json.loads(jsonEvents)
        header = {'Authorization': token,'Content-type':'application/json', 'Accept':'application/json'}
        result = requests.post(server, json=json_data, headers=header)
        if result.status_code == 201:
            os.remove(pathA + "/captures/" + projectName + "/" + captureName + "/Events.json")
            json_file_concat_list_reset(len(jsonEvents))
            return True
        else:
            return False
    except:
        return False

'''
Post screenshots to the server API as a multipart form.
The screenshots are send one by one. 
We send a value to know if it is the last capture sent to change the value of saveScreenshot in the server 
when the capture is finished.
'''
def post_screenshot(jsonScreenshot):
    global token
    try:
        server = server_url()+"screenshots/"
        projectName = project_name()
        captureName = capture_name()
        imageLimit = send_policy("imageLimit")
        pathA = os.getcwd()
        if not imageLimit:
            if jsonScreenshot is None:
                try:
                    f = open(pathA+"/captures/" + projectName +"/"+captureName+"/Screenshots.json", "r")
                    lines_events = f.readlines()
                    f.close()
                    jsonScreenshot = []
                    for lines in lines_events:
                        new_line = ast.literal_eval(lines.strip())
                        jsonScreenshot += new_line
                    jsonScreenshot = json.loads(json.dumps(jsonScreenshot))
                except:
                    return False
            screenshots = json.loads(jsonScreenshot)
            count = 0
            tam = len(screenshots)
            for i in screenshots:
                count = count+1
                if count == tam:
                    rest=True
                else:
                    rest = False
                file = {
                        'timestamp': str(i['timestamp']),
                        'rest': str(rest),
                        'imagePath': ((captureName+"/"+str(i['timestamp'])+".png"), open(i['imagePath'], 'rb'), 'text/plain')
                        }
                multipart_data = MultipartEncoder(file)
                header = {'Authorization': token,'Content-Type': multipart_data.content_type}
                result = requests.post(server,data=multipart_data, headers=header)
                if result.status_code == 201:
                    if count == tam:
                        os.remove(pathA+"/captures/" + projectName +"/"+captureName+"/Screenshots.json")
                        for i in screenshots:
                            imagePath = i["imagePath"]
                            os.remove(imagePath)
                        json_file_imagen_list_reset(tam)
                        return True
                else:
                    return False
    except:
        return False

'''
Thread to check connection status according to shipping policy
'''
def control_connection():
    global stop_thread_connection
    if not stop_thread_connection:
        connection(True)
        ticker = threading.Timer(send_policy("connectionTime"), control_connection).start()
    else:
        return None
'''
Stop thread connection
'''
def stop_connection():
    global stop_thread_connection
    stop_thread_connection = True

'''
Get thread connection status
'''
def get_thread_status():
    global stop_thread_connection
    return stop_thread_connection

